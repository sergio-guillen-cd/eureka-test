build-server:
	$(MAKE) -C ./eurekaserver build

build-app1:
	$(MAKE) -C ./eurekaapp1 build

build-app2:
	$(MAKE) -C ./eurekaapp2 build

build: build-server build-app1 build-app2

start:
	docker-compose up -d
