How to build and run
===

This example application has the next components:
- Eureka server: used for service discovery.
- Eureka App1: it has an endpoint that calls App2 using eureka client discovery.
- Eureka App2: sample microservice used by App1

In order to run locally you need to have the next installed:
- Apache Maven
- Docker
- Docker Compose
- GNU Make

## Building
```
# Building all applications and their docker images
$ make build
```

```
# Builing eureka server and its docker image
$ make build-server
```

```
# Builing eureka app1 and its docker image
$ make build-app1
```

```
# Builing eureka app2 and its docker image
$ make build-app2
```

## Starting local environment
```
# Starting all applications
$ make start

# Bringing environment down
$ docker-compose down
```
