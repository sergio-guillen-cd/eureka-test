package com.example.eurekaapp2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Eurekaapp2Application {

	public static void main(String[] args) {
		SpringApplication.run(Eurekaapp2Application.class, args);
	}

}
