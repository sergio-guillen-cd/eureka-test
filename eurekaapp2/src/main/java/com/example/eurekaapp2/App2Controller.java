package com.example.eurekaapp2;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class App2Controller {

    @GetMapping("/callClientTwo")
    public ResponseEntity<String> greeting() throws Exception {
        String response = "Response from app2";
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }
}
