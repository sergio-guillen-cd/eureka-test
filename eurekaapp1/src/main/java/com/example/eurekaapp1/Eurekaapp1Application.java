package com.example.eurekaapp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

// @EnableEurekaClient
@SpringBootApplication
@EnableDiscoveryClient
public class Eurekaapp1Application {

	public static void main(String[] args) {
		SpringApplication.run(Eurekaapp1Application.class, args);
	}

}
