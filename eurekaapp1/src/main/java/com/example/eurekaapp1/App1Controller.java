package com.example.eurekaapp1;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.ApplicationInfoManager;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.appinfo.MyDataCenterInstanceConfig;
import com.netflix.discovery.DiscoveryClient;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.transport.DefaultEurekaTransportConfig;

@RestController
public class App1Controller {
    private Logger log = LoggerFactory.getLogger(App1Controller.class);

    @Autowired
    private LoadBalancerClient loadBalancerClient;
    private RestTemplate restTemplate = new RestTemplate();

    private final String APP2_SERVICE_NAME = "app2";

    @GetMapping("/callClientOne")
    public ResponseEntity<String> callClientOne() {
        String response = "Response from app1";
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping("/callClientTwoThroughClientOne")
    public ResponseEntity<String> callClientTwoThroughClientOne() {
        // String endpoint = getBaseUrl();
        String endpoint = getEndpoint();
        try {
            return new ResponseEntity<String>(restTemplate.getForObject(endpoint + "/callClientTwo", String.class), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(restTemplate.getForObject(endpoint + "/callClientTwo", String.class), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // private String getBaseUrl() {
    //     ServiceInstance instance = loadBalancerClient.choose(APP2_SERVICE_NAME);
    //     String uri = instance.getUri().toString();
    //     log.info("APP2 URI: " + uri);
    //     return uri;
    // }

    @Autowired
    private EurekaClient eurekaClient;

    private InstanceInfo getServiceInstance(String serviceName) {
        return eurekaClient.getNextServerFromEureka(serviceName, false);
    }

    private String getEndpoint() {
        InstanceInfo instanceInfo = getServiceInstance(APP2_SERVICE_NAME);
        if (instanceInfo == null) {
            return null;
        }
        String endpoint = String.format("http://%s:%d", instanceInfo.getIPAddr(), instanceInfo.getPort());
        log.info("APP2 endpoint: " + endpoint);
        return endpoint;
    }
}
